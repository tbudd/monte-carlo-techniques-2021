{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "41d26cde",
   "metadata": {},
   "source": [
    "# Exercise sheet 6 (Wed 13 Oct, graded)\n",
    "\n",
    "Some general remarks about the exercises:\n",
    "* For your convenience the functions from the lecture are included below. Feel free to reuse them without copying to the exercise solution box.\n",
    "* For each part of the exercise a single solution box has been added, but you may insert additional boxes above or below using `Insert > Insert cell above / below`. Do not hesitate to add Markdown boxes for textual or LaTeX answers (via `Cell > Cell Type > Markdown`).\n",
    "* Please make your code readable by humans (and not just by the Python interpreter): choose informative function and variable names and use consistent formatting. Feel free to check the [PEP 8 Style Guide for Python](https://www.python.org/dev/peps/pep-0008/) for the widely adopted coding conventions or [this guide for explanation](https://realpython.com/python-pep8/).\n",
    "* Make sure that the full notebook runs without errors before submitting your work. This you can do by selecting `Kernel > Restart & Run All` in the jupyter menu.\n",
    "* This exercise sheet __will be graded__. The maximal number of points is **10** and a subdivision of these points for each exercise is indicated. Please submit your completed notebook before __Wednesday 20 Oct 10:30__ by saving the notebook to your local computer via `File > Download as > Notebook (.ipynb)` and uploading it to the appropriate Brightspace Assignment.\n",
    "* If you have submitted your solutions in time and your grade is below a 7.0, you have the option to resubmit your solutions within a week (**correction:** two weeks this time, because of autumn holiday), so before Wednesday 3 Nov 10:30. Then they will be regraded, but with a cap of 7.0 on the grade.\n",
    "\n",
    "### Code from the lecture"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb41d2a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pylab as plt\n",
    "\n",
    "rng = np.random.default_rng()\n",
    "%matplotlib inline\n",
    "\n",
    "\n",
    "def aligned_init_config(width):\n",
    "    '''Produce an all +1 configuration.'''\n",
    "    return np.ones((width,width),dtype=int)\n",
    "\n",
    "def plot_ising(config,ax,title):\n",
    "    '''Plot the configuration.'''\n",
    "    ax.matshow(config, vmin=-1, vmax=1, cmap=plt.cm.binary)\n",
    "    ax.title.set_text(title)\n",
    "    ax.set_yticklabels([])\n",
    "    ax.set_xticklabels([])\n",
    "    ax.set_yticks([])\n",
    "    ax.set_xticks([])\n",
    "\n",
    "from collections import deque\n",
    "\n",
    "def neighboring_sites(s,w):\n",
    "    '''Return the coordinates of the 4 sites adjacent to s on an w*w lattice.'''\n",
    "    return [((s[0]+1)%w,s[1]),((s[0]-1)%w,s[1]),(s[0],(s[1]+1)%w),(s[0],(s[1]-1)%w)]\n",
    "\n",
    "def cluster_flip(state,seed,p_add):\n",
    "    '''Perform a single Wolff cluster move with specified seed on the state with parameter p_add.'''\n",
    "    w = len(state)\n",
    "    spin = state[seed]\n",
    "    state[seed] = -spin  \n",
    "    cluster_size = 1\n",
    "    unvisited = deque([seed])   # use a deque to efficiently track the unvisited cluster sites\n",
    "    while unvisited:   # while unvisited sites remain\n",
    "        site = unvisited.pop()  # take one and remove from the unvisited list\n",
    "        for nbr in neighboring_sites(site,w):\n",
    "            if state[nbr] == spin and rng.uniform() < p_add:\n",
    "                state[nbr] = -spin\n",
    "                unvisited.appendleft(nbr)\n",
    "                cluster_size += 1\n",
    "    return cluster_size\n",
    "\n",
    "def wolff_cluster_move(state,p_add):\n",
    "    '''Perform a single Wolff cluster move on the state with addition probability p_add.'''\n",
    "    seed = tuple(rng.integers(0,len(state),2))\n",
    "    return cluster_flip(state,seed,p_add)\n",
    "\n",
    "def sample_autocovariance(x,tmax):\n",
    "    '''Compute the autocorrelation of the time series x for t = 0,1,...,tmax-1.'''\n",
    "    x_shifted = x - np.mean(x)\n",
    "    return np.array([np.dot(x_shifted[:len(x)-t],x_shifted[t:])/len(x) for t in range(tmax)])\n",
    "\n",
    "def find_correlation_time(autocov):\n",
    "    '''Return the index of the first entry that is smaller than autocov[0]/e.'''\n",
    "    goal = np.exp(-1)*autocov[0]\n",
    "    for t in range(len(autocov)):\n",
    "        if autocov[t] <= goal:\n",
    "            return t\n",
    "    # failed to find\n",
    "    return len(autocov)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3317e002",
   "metadata": {},
   "source": [
    "## 6.1 MCMC simulation of the XY model (10 Points)\n",
    "\n",
    "_Goal of this exercise_: Practice implementing MCMC simulation of the XY spin model using an appropriate cluster algorithm and analyzing the numerical effectiveness.\n",
    "\n",
    "The **XY model** is a relative of the Ising model in which the discrete $\\pm 1$ spin at each lattice site is replaced by a continuous $2$-dimensional spin on the unit circle \n",
    "\n",
    "$$S_1 = \\{(x,y)\\in\\mathbb{R}^2: x^2+y^2=1\\}.$$\n",
    "\n",
    "To be precise, we consider a $w\\times w$ lattice with periodic boundary conditions and a XY configuration $s = (s_1,\\ldots,s_N) \\in \\Gamma = S_1^N$, $N=w^2$, with Hamiltonian that is very similar to the Ising model,\n",
    "\n",
    "$$H_{XY}(s) = - J \\sum_{i\\sim j} s_i \\cdot s_j.$$\n",
    "\n",
    "Here as in the Ising model the sum is over nearest neighbor sites $i$ and $j$ and $s_i \\cdot s_j$ is the usual Euclidean inner product of the vectors $s_i,s_j \\in S_1$. We will only consider the ferromagnetic XY model and set $J=1$ in the remainder of the exercise. Note that nowhere in the definition the $x$- and $y$-components of the spins are related to the two directions of the lattice (one could also have studied the XY model on a one-dimensional or three-dimensional lattice and the spins would still have two components). As usual we are interested in sampling configurations $s\\in \\Gamma$ with distribution $\\pi(s)$ given by the Boltzmann distribution\n",
    "$$ \\pi(s) = \\frac{1}{Z_{XY}} e^{-\\beta H_{XY}(s)}, \\qquad \\beta = 1/T.$$\n",
    "\n",
    "The XY model admits a (local) cluster algorithm that is very similar to the Wolff algorithm of the Ising model. It amounts to the following recipe:\n",
    "1. Sample a uniform seed site $i_{\\text{seed}}$ in $1,\\ldots,N$ and an independent uniform unit vector $\\hat{n} \\in S_1$.\n",
    "2. Grow a cluster $C$ starting from the seed $i_{\\text{seed}}$ consisting only of sites $j$ whose spin $s_j$ is  \"aligned\" with the seed, in the sense that $s_j\\cdot\\hat{n}$ has the same sign as  $s_{i_{\\text{seed}}}\\cdot \\hat{n}$. Like in the Ising model this is done iteratively by examining the neighbors of sites that are already in the cluster, and adding those that are aligned with appropriate probability. The difference with the Ising model is that this probability depends on the spins $s_i$ and $s_j$ that are linked (meaning that $s_j$ is an aligned neighbor of $s_i$) via the formula\n",
    "$$ p_{\\text{add}}(s_i,s_j) = 1 - \\exp\\big( -2\\beta\\,(s_i\\cdot\\hat{n})(s_j\\cdot\\hat{n})\\big).$$\n",
    "3. Once the cluster $C$ is constructed, all of its spins are \"flipped\" in the sense that they are reflected in the plane perpendicular to $\\hat{n}$, i.e. $s_j \\to s_j - 2(s_j\\cdot\\hat{n})\\hat{n}$.\n",
    "\n",
    "__(a)__ Verify by hand that the probabilities $p_{\\text{add}}(s_i,s_j)$ are the appropriate ones to ensure that detailed balance for the Boltzmann distribution $\\pi(s)$. For this you may restrict your attention to the probabilities associated with the boundary edges in the cluster $C$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b03f820b",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "66a9278e",
   "metadata": {},
   "source": [
    "__(b)__ Implement the cluster algorithm as described above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e4a6d63",
   "metadata": {},
   "outputs": [],
   "source": [
    "def xy_cluster_flip(state,seed,nhat,beta):\n",
    "    '''Perform a cluster move on the state and return the cluster size.\n",
    "\n",
    "    Keyword arguments:\n",
    "    state -- a numpy array of dimensions (w,w,2) representing the spins\n",
    "    seed -- tuple i,j describing the index of the seed site\n",
    "    nhat -- numpy array of dimensions (2) describing a unit 2-vector\n",
    "    beta -- inverse temperature\n",
    "    '''\n",
    "    # Your code here\n",
    "    return cluster_size\n",
    "\n",
    "def xy_cluster_move(state,beta):\n",
    "    '''Perform a single random cluster move.\n",
    "    \n",
    "    Keyword arguments:\n",
    "    state -- a numpy array of dimensions (w,w,2) representing the spins\n",
    "    beta -- inverse temperature\n",
    "    '''\n",
    "    seed = ...\n",
    "    nhat = ...\n",
    "    return xy_cluster_flip(state,seed,nhat,beta)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "daa0b50e",
   "metadata": {},
   "source": [
    "__(c)__ Estimate and plot the average cluster size in equilibrium for a 25x25 lattice ($w=25$) for the range of temperatures $T = 0.5,0.6,\\ldots,1.5$. It is not necessary first to estimate the equilibration time: you may start in a fully aligned state and use ~200 moves for equilibration and ~500 for estimating the average cluster size. (To check your algorithm: the average cluster size at $T=0.8$ should be around 270.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f91dd67",
   "metadata": {},
   "outputs": [],
   "source": [
    "temperatures = np.linspace(0.5,1.5,11)\n",
    "width = 25\n",
    "equilibration_moves = 200\n",
    "measurement_moves = 500\n",
    "\n",
    "# your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd5fa10f",
   "metadata": {},
   "source": [
    "__(d)__ Make an MCMC estimate (and plot!) of the **mean square magnetization per spin** $\\mathbb{E}[ m^2(s) ]$ for the same set of temperatures, where $$m^2(s) = \\left(\\frac{1}{N}\\sum_{i=1}^N s_i\\right)\\cdot\\left(\\frac{1}{N}\\sum_{i=1}^N s_i\\right).$$ To choose the equilibration time and time between measurement, use the average cluster size from (c) to estimate how many moves correspond to 1 _sweep_, i.e. roughly $N = w^2$ updates to spins. Then use 100 equilibration _sweeps_ and 200 measurements of $m^2(s)$, with 2 _sweeps_ between each measurement. Check whether your measurements still have significant autocorrelation, and estimate the rough error in $\\mathbb{E}[ m^2(s) ]$ based on this. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c4e5085",
   "metadata": {},
   "outputs": [],
   "source": [
    "measurements = 200\n",
    "equil_sweeps = 100\n",
    "measure_sweeps = 2\n",
    "\n",
    "def squared_magnetization(state):\n",
    "    # code\n",
    "    return msquared\n",
    "\n",
    "# code"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "326cc9c7",
   "metadata": {},
   "source": [
    "__(e)__ Produce pictures/snapshots of equilibrated XY states at temperatures $T = 0.6, 0.9, 1.2, 1.5$. You may use the provided plotting function, which shows colors based on the angle of the spin. Can you observe the [**Kosterlitz–Thouless transition** of the XY model](https://en.wikipedia.org/wiki/Classical_XY_model)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42255f8e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_xy(state,ax,title=\"\"):\n",
    "    '''Plot the XY configuration.'''\n",
    "    angles = np.arctan2(*np.transpose(state,axes=(2,0,1)))\n",
    "    ax.matshow(angles, vmin=0, vmax=2*np.pi, cmap=plt.cm.hsv)\n",
    "    ax.title.set_text(title)\n",
    "    ax.set_yticklabels([])\n",
    "    ax.set_xticklabels([])\n",
    "    ax.set_yticks([])\n",
    "    ax.set_xticks([])\n",
    "\n",
    "# code\n",
    "\n",
    "# for example\n",
    "fig, axes = plt.subplots(1,4,figsize=(16,5))\n",
    "for i,ax in enumerate(axes):\n",
    "    state = ... \n",
    "    plot_xy(state,ax,\"T = {}\".format([0.6,0.9,1.2,1.5][i]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35aaad93",
   "metadata": {},
   "source": [
    "## Feedback\n",
    "\n",
    "Please use the following Google form to give anonymous and constructive feedback on the lecture and/or these exercises. This is an easy way to contribute to improving the course: https://forms.gle/NdM7rqDW5z1HkngCA"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
