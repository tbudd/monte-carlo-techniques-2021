#ifndef MAPGENERATORS_H
#define MAPGENERATORS_H

#include "planarmap.h"
#include "random.h"

#include <stack>

using namespace planar;


typedef typename Map<EdgeData>::EdgeHandle EdgeHandle;

enum walkSteps { RIGHT, LEFT, UP, DOWN };

enum ModelType { SPANNINGTREE, UNIFORM, SCHNYDER, BIPOLAR };

template<typename RNG>
std::vector<int> randomDyckPath(int halflength, RNG &rng) {
	std::vector<int> steps;
	steps.reserve(2*halflength);
	int q = halflength, p = halflength;

	for (int i = 0; i < 2*halflength; ++i) {
		// Cannot use uniform_int_distribution here, since (q+p)*(q-p+1) may overflow.
                double uniform = uniform_real(rng);
		double probability = double(q+1)*(q-p)/(double(q+p)*(q-p+1));
		if (uniform < probability) {
			steps.push_back(-1);
			--q;
		} else {
			steps.push_back(1);
			--p;
		}
	}
	return steps;
}

inline std::vector<int> matchingFromDyckPath(std::vector<int> &path) {
	std::stack<int> stack;
	std::vector<int> matching;
	matching.reserve(path.size());
	for (int i = 0, endi = path.size(); i < endi; ++i) {
		if (path[i] == 1) {
			stack.push(i);
			matching.push_back(i);
		} else {
			matching.push_back(stack.top());
			matching[matching.back()] = i;
			stack.pop();
		}
	}
	return matching;
}

inline void addFirstSchnyderTree(std::vector<walkSteps> const &walk, Map<EdgeData> &map) {
	EdgeHandle curEdge = map.getRoot();
	int size = walk.size();
	for (int i = 0; i < size; ++i) {
		switch (walk[i]) {
			case RIGHT:
			case DOWN:
				// the equivalent of an up-step in Dyck path
				curEdge = map.insertEdge(curEdge)->getNext();
				break;
			case UP:
			case LEFT:
				// the equivalent of a down-step in Dyck path
				curEdge = curEdge->getNext();
				break;
		}
	}
}

inline std::vector<EdgeHandle> addSecondSchnyderTree(std::vector<walkSteps> const &walk, Map<EdgeData> &map) {
	// start with first edge of existing tree
	EdgeHandle curEdge = map.getRoot()->getNext(3);
	int size = walk.size();

	std::queue<int> numheads;
	numheads.push(0);
	for (int i = 0; i < size; ++i) {
		if (walk[i] == RIGHT || walk[i] == UP) {
			numheads.push(0);
		} else {
			++numheads.back();
		}
	}

	std::stack<EdgeHandle> tails;
	std::vector<EdgeHandle> greencorners;
	for (int i = 1; i <= size + 1; ++i) {
		curEdge = curEdge->getNext();

		if (i == size + 1 || walk[i - 1] == RIGHT || walk[i - 1] == DOWN) {
			// curEdge corresponds to first corner of a vertex
			int heads = numheads.front();
			numheads.pop();
			//assert(heads <= static_cast<int>(tails.size()));
			while (heads > 0) {
				map.insertEdge(tails.top(), curEdge);
				tails.pop();
				heads--;
			}
			if (i < size) {
				greencorners.push_back(curEdge->getPrevious());
			}
		}

		if (i < size && (walk[i] == UP || walk[i] == LEFT)) {
			// equiv of Dyck down step
			// curEdge corresponds to last corner of a vertex
			tails.push(curEdge);
		}
	}
	//assert(tails.empty());
	//assert(numheads.empty());
	return greencorners;
}

template <typename RNG>
std::vector<walkSteps> randomWalkInCone(int halflength, RNG &rng) {
	// produce a walk in 45° cone, i.e. (0, 0) → (0, 0) such that 0 ≤ y ≤ x.

	std::vector<walkSteps> walk;
	walk.reserve(2*halflength);

	int x = 0, y = 0;
	for (int n = 2*halflength; n != 0; --n) {
		// Probabilities chosen so all walks have equal probability. Although these are integers, they are way too big for an uint64_t.
		double pRight = double(3 + x)*(n - x - y)*(2 + x - y)*(1 + y)*(2 + n - x + y)*(4 + x + y);
		double pLeft = double(1 + x)*(x - y)*(4 + n + x - y)*(1 + y)*(2 + x + y)*(6 + n + x + y);
		double pUp = double(2 + x)*(x - y)*(4 + n + x - y)*(2 + y)*(4 + x + y)*(n - x - y);
		double pDown = double(2 + x)*(2 + x - y)*y*(2 + n - x + y)*(2 + x + y)*(6 + n + x + y);
                double total = pRight+pLeft+pUp+pDown;
                
                double uniform = uniform_real(rng,0,total);
                walkSteps dir;
                if( uniform < pRight )
                {
                    dir = RIGHT;
                } else if( uniform < pRight + pLeft )
                {
                    dir = LEFT;
                } else if( uniform < pRight + pLeft + pUp )
                {
                    dir = UP;
                } else
                {
                    dir = DOWN;
                }
		walk.push_back(dir);
		switch (dir) {
			case RIGHT: ++x; break;
			case LEFT: --x; break;
			case UP: ++y; break;
			case DOWN: --y; break;
		}
	}

	return walk;
}

template <typename RNG>
void makeSchnyderMap(int size, Map<EdgeData> & map, RNG &rng) {
	if (size%2 == 1) {
		++size;
	}
	size = size/2 - 1;
	std::vector<walkSteps> walk = randomWalkInCone(size, rng);

	map.clearMap();
	map.makePolygon(3);
	addFirstSchnyderTree(walk,map);
	std::vector<EdgeHandle> greencorners = addSecondSchnyderTree(walk,map);
	for (std::vector<EdgeHandle>::iterator corner = greencorners.begin(); corner != greencorners.end(); ++corner) {
		map.insertEdge((*corner)->getNext(),(*corner)->getPrevious());
	}
}


template <typename RNG>
std::vector<walkSteps> randomSkewWalkInCone(int length, RNG &rng) {
	// produce a uniform walk in the first quadrant with steps UP=(0, 1), LEFT=(-1, 0), RIGHT(down)=(1, -1)
	// starting and ending at the origin

	std::vector<walkSteps> walk;
	walk.reserve(length);

	int x = 0, y = 0;
	for (int n = length; n != 0; --n) {
		double pRightDown = double(2 + x)*y*(3 + n - x + y)*(2 + x + y);
		double pLeft = double(1 + y)*x*(1 + x + y)*(6 + n + 2*x + y);
		double pUp = double(1 + x)*(n - x - 2*y)*(2 + y)*(3 + x + y);
        double total = pRightDown + pLeft + pUp;
        walkSteps dir;
        
        double uniform = uniform_real(rng,0,total);
        if( uniform < pRightDown )
        {
            dir = RIGHT;
        } else if( uniform < pRightDown + pLeft )
        {
            dir = LEFT;
        } else
        {
            dir = UP;
        }
 		walk.push_back(dir);
		switch (dir) {
			case RIGHT: ++x; --y; break; // RIGHT is right & down
			case LEFT: --x; break;
			case UP: ++y; break;
			case DOWN: break;
		}
	}

	return walk;
}

template <typename RNG>
std::vector<walkSteps> randomKrewerasExcursion(int length, RNG &rng) {
	// produce a uniform walk with steps UP=(0, 1), RIGHT=(1, 0), DOWN(left)=(-1, -1)
	// starting at the origin, ending on the diagonal x+y=0, and staying in x+y >= 0
    
        std::vector<walkSteps> walk(length);
        int h = 0;
        for(int n=length;n!=0;--n) {
            // produce the walk in reverse:
            // The number of Kreweras walks with steps (1,1), (-1,0), (0,1) of length 
            // n from (x,y) with x+y=h to the diagonal x+y=0 is 2^{(2l+h)/3} (h+1)/(l+1)\binom{l+1}{(l-h)/3}, 
            // so the probability of a (1,1) step is (h+3)*(l-h)) / ((h+1)*l*3).
            double plus2prob = (double(h+3)*(n-h)) / (double(h+1)*n*3);
            if( random_bernoulli(rng, plus2prob) )
            {
                walk[n-1] = DOWN;
                h += 2;
            } else if( random_bernoulli(rng, 0.5) )
            {
                walk[n-1] = UP;
                h -= 1;
            } else 
            {
                walk[n-1] = RIGHT;
                h -= 1;
            }
        }
	return walk;
}

inline
void walkToBipolarMap(std::vector<walkSteps> const &walk, Map<EdgeData> &map) {
	map.clearMap();
	map.newDoubleEdge();
	map.setRoot(map.getEdge(0));
	EdgeHandle curEdge = map.getRoot()->getNext();
	int size = walk.size();
	for (int i = 0; i < size - 1; ++i) {
		switch (walk[i]) {
			case UP:
				curEdge = map.insertEdge(curEdge->getNext())->getAdjacent();
				break;
			case LEFT:
				curEdge = map.insertEdge(curEdge, curEdge->getNext(2));
				break;
			case RIGHT:
				curEdge = map.insertEdge(curEdge->getPrevious(2), curEdge);
				break;
			case DOWN:
				break;
		}
	}
}

inline
void walkToPercolatedTriangulation(std::vector<walkSteps> const &walk, Map<EdgeData> &map) {
    
    // uses Bernardi's bijection (arxiv:math/0605320)
    map.clearMap();
    map.newDoubleEdge();
    map.setRoot(map.getEdge(0));
    EdgeHandle curEdge = map.getRoot()->getNext();
    int size = walk.size();
    for (int i = 0; i < size; ++i) {
        switch (walk[i]) {
            case UP:
                    map.insertEdge(curEdge)->data().vertexId = i;
                    curEdge = map.insertEdge(curEdge->getPrevious(),curEdge->getNext());
                    break;
            case RIGHT:
                    map.insertEdge(curEdge->getNext())->data().vertexId = i;
                    curEdge = map.insertEdge(curEdge,curEdge->getNext(2));
                    break;
            case DOWN:
                    // determine oldest boundary edge, and glue curEdge to that
                    if( curEdge->getPrevious()->data().vertexId < curEdge->getNext()->data().vertexId )
                    {
                        // left is older, next curEdge is the one previously to the right 
                        curEdge = map.glueEdges(curEdge,curEdge->getPrevious()).front(); 
                    } else
                    {
                        // right is older, next curEdge is the one previously to the left
                        curEdge =  map.glueEdges(curEdge,curEdge->getNext()).front()->getPrevious();
                    }
                    break;
            case LEFT:
                    break;
        }
    }
    // now an outerface of degree 2 should be left that we remove by gluing its sides together
    map.glueEdges(curEdge,curEdge->getNext());
}

template<typename RNG>
void makeUniformTriangulation(int size, Map<EdgeData>& map, RNG &rng) {
    int walklength = 3*size/2;
    std::vector<walkSteps> walk = randomKrewerasExcursion(walklength, rng);
    
    walkToPercolatedTriangulation(walk,map);
}

void walkToTriangulation(std::vector<walkSteps> const &walk, Map<EdgeData> &map) {
	map.clearMap();
	map.newDoubleEdge();
	map.setRoot(map.getEdge(0));
	EdgeHandle curEdge = map.getRoot()->getNext();
	int size = walk.size();
	for (int i = 0; i < size - 1; ++i) {
		switch (walk[i]) {
			case UP:
                                map.insertEdge(curEdge);
				curEdge = map.insertEdge(curEdge->getPrevious(),curEdge->getNext());
				break;
			case DOWN:
				curEdge = map.insertEdge(curEdge->getNext(), curEdge->getPrevious())->getAdjacent();
				break;
			case RIGHT:
                                map.insertEdge(curEdge->getNext());
				curEdge = map.insertEdge(curEdge,curEdge->getNext(2));
				break;
			case LEFT:
				curEdge = map.insertEdge(curEdge, curEdge->getNext(2));
				break;
		}
	}
}

template<typename RNG>
void makeBipolarMap(int size, Map<EdgeData> & map, RNG &rng) {
	int numEdges = 3*size/2;

	std::vector<walkSteps> walk = randomSkewWalkInCone(numEdges, rng);
    
    

	walkToBipolarMap(walk,map);
}

template<typename RNG>
std::vector<bool> randomSubsetIndicator(int length, int number, RNG &rng) {
	std::vector<bool> indicator;
	indicator.reserve(length);
	int rest = number;
	for (int l = length; l > 0; --l) {
                int p = 1+uniform_int(rng,l);
		if (p <= rest) {
			indicator.push_back(true);
			rest--;
		} else {
			indicator.push_back(false);
		}
	}
	return indicator;
}

template<typename RNG>
std::vector<walkSteps> randomWalkInQuarterPlane(int halflength, RNG &rng) {
	// We want to generate a walk in qplane by shuffling two walks on half-line.
	// The precise distribution of the first walk is binom{2n}{2k}*cat(n-k)*cat(k)/(cat(n)*cat(n+1)).
	// To approximate this we generate a normal random variable with mean n/2 and st.dev. sqrt(n/8).
        int k = static_cast<int>(0.5 + random_normal(rng,0.5*halflength,std::sqrt(halflength/8.0)));
	k = std::min(std::max(k, 0), halflength);

	std::vector<int> steps1 = randomDyckPath(k, rng);
	std::vector<int> steps2 = randomDyckPath(halflength - k, rng);
	std::vector<bool> indicator = randomSubsetIndicator(2*halflength, 2*k, rng);

	int num1 = 0, num2 = 0;
	std::vector<walkSteps> steps;
	steps.reserve(2*halflength);
	for (int i = 0; i < 2*halflength; ++i) {
		if (indicator[i]) {
			steps.push_back(steps1[num1] == 1? UP : DOWN);
			++num1;
		} else {
			steps.push_back(steps2[num2] == 1? RIGHT : LEFT);
			++num2;
		}
	}
	return steps;
}

template <typename RNG>
void makeSpanningTreeMap(int size, Map<EdgeData> & map, RNG &rng) {
    int walksize = size/2;
    std::vector<walkSteps> walk = randomWalkInQuarterPlane(walksize, rng);

    walkToTriangulation(walk, map);
}

template <typename RNG>
void makeRandomMap(ModelType type, int size, Map<EdgeData> & map, RNG &rng) {
    switch (type) {
        case SPANNINGTREE:
                makeSpanningTreeMap(size,map,rng);
                break;
        case UNIFORM:
                makeUniformTriangulation(size,map,rng);
                break;
        case SCHNYDER:
                makeSchnyderMap(size,map,rng);
                break;
        case BIPOLAR:
                makeBipolarMap(size,map,rng);
                break;
    }
}

// Assign vertex ids to the edges in the map and return number of vertices.
int assignVertexIds(Map<EdgeData> & map)
{
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().vertexId = -1;
	}
	
	int nextId = 0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().vertexId == -1 )
		{
			EdgeHandle vertexEdge = *it;
			do {
				vertexEdge->data().vertexId = nextId;
				vertexEdge = vertexEdge->getAdjacent()->getNext();
			} while (vertexEdge != *it);
			nextId++;
		}
	}
	return nextId;
}





#endif
