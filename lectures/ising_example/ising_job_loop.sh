#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=100M
#SBATCH --time=2:00:00
cd ~/monte-carlo-techniques-2021/lectures/ising_example
for temp in $(LANG=en_US seq 1.5 0.1 3.5)
do
	/software/anaconda3/bin/python3 ising.py -w 10 -t ${temp} -n 50
done
