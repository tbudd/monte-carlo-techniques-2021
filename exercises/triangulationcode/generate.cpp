#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdlib.h>

#include "planarmap.h"


using namespace planar;

// Every oriented edge will carry the following data
struct EdgeData {
        int vertexId; // used in mapgenerators.h
		int triangulation_id; // new id that be set after generation
};

#include "mapgenerators.h"

void set_triangulation_ids(Map<EdgeData> & map) {
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++) {
		(*it)->data().triangulation_id = -1;
	}	
	int index = 0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++) {
		if( (*it)->data().triangulation_id == -1 )
		{
			(*it)->data().triangulation_id = index;
			(*it)->getNext()->data().triangulation_id = index+1;
			(*it)->getPrevious()->data().triangulation_id = index+2;
			index += 3;
		}
	}	
}

void exportTriangulation(Map<EdgeData> & map, std::ostream & stream)
{
    std::vector<EdgeHandle > edges(map.numHalfEdges());
    for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
        edges[(*it)->data().triangulation_id] = *it;
	}
	for(EdgeHandle edge: edges)
	{
		stream << edge->getAdjacent()->data().triangulation_id << "\n";
	}
}

int main(int argc, char **argv)
{
	// initialize random number generator
	Xoshiro256PlusPlus rng(getseed());
	
	std::string typeletter = "U";
	int size = 30;

	// parse command line arguments (the naive way)
	for( int i=1;i<argc;i++)
	{
		if( argv[i][0] != '-' )
			continue;
		if( argv[i][1] == 't')
		{
			typeletter = argv[i][2];
		} else if( argv[i][1] == 's' )
		{
			std::istringstream param(argv[i]+2);
			param >> size;
		}
	}
	
    ModelType modeltype = SPANNINGTREE;
	if( typeletter == "S" )
	{
		modeltype = SPANNINGTREE;
	} else if( typeletter == "U" )
	{
		modeltype = UNIFORM;
	} else if( typeletter == "W" )
	{
		modeltype = SCHNYDER;
	} else if( typeletter == "B" )
	{
		modeltype = BIPOLAR;
	} else
        {
		std::cout << "Model type unknown. Select S (spanning-tree-decorated), U (uniform), W (schnyder-wood-decorated), B (bipolar-oriented)!\n";
		return 1;
	}
	
	if( size <= 0 || size%2==1 )
	{
		std::cout << "The number of triangles should be an even positive integer!\n";
		return 1;
	}
	
	
	Map<EdgeData> map;
    makeRandomMap(modeltype,size,map,rng);
    set_triangulation_ids(map);
    exportTriangulation(map,std::cout);
	
	return 0;
}
