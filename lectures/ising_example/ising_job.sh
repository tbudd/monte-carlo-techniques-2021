#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=100M
#SBATCH --time=2:00:00
cd ~/monte-carlo-techniques-2021/lectures/ising_example
/software/anaconda3/bin/python3 ising.py -w 30 -t 2.5 -n 100
