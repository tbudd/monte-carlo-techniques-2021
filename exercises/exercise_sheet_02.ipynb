{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "41d26cde",
   "metadata": {},
   "source": [
    "# Exercise sheet 2 (Wed 15 Sept, graded)\n",
    "\n",
    "Some general remarks about the exercises:\n",
    "* For your convenience the functions from the lecture are included below. Feel free to reuse them without copying to the exercise solution box.\n",
    "* For each part of the exercise a single solution box has been added, but you may insert additional boxes above or below using `Insert > Insert cell above / below`. Do not hesitate to add Markdown boxes for textual or LaTeX answers (via `Cell > Cell Type > Markdown`).\n",
    "* Please make your code readable by humans (and not just by the Python interpreter): choose informative function and variable names and use consistent formatting. Feel free to check the [PEP 8 Style Guide for Python](https://www.python.org/dev/peps/pep-0008/) for the widely adopted coding conventions or [this guide for explanation](https://realpython.com/python-pep8/).\n",
    "* Make sure that the full notebook runs without errors before submitting your work. This you can do by selecting `Kernel > Restart & Run All` in the jupyter menu.\n",
    "* This exercise sheet __will be graded__. The maximal number of points is **10** and a subdivision of these points for each exercise is indicated. Please submit your completed notebook before __Monday 20 Sept 13:30__ by saving the notebook to your local computer via `File > Donwload as > Notebook (.ipynb)` and uploading it to the appropriate Brightspace Assignment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "cb41d2a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pylab as plt\n",
    "\n",
    "rng = np.random.default_rng()\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3317e002",
   "metadata": {},
   "source": [
    "## 2.1 Sampling random variables via the inversion method (3 Points)\n",
    "\n",
    "Recall from the lecture that for any real random variable $X$ we can construct an explicit random variable via the inversion method that is identically distributed. This random variable is given by $F_X^{-1}(U)$ where $F_X$ is the CDF of $X$ and $U$ is a uniform random variable on $(0,1)$ and \n",
    "\n",
    "$$\n",
    "F_X^{-1}(p) := \\inf\\{ x\\in\\mathbb{R} : F_X(x) \\geq p\\}.\n",
    "$$\n",
    "\n",
    "This gives a very general way of simulating $X$ in a computer program, as you will find out in this exercise.\n",
    "\n",
    "__(a)__ Let $X$ be a discrete random variable taking values in $\\{1,2,\\ldots,n\\}$. Write a Python function that takes the probability function $p_X$ as a list and returns a random sample with the distribution of $X$ using the inversion method. Verify the working of your function numerically on an example. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "210f1302",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "d590b09d",
   "metadata": {},
   "source": [
    "__(b)__ Let now $X$ be an **exponential random variable** with **rate** $\\lambda$, i.e. a continuous random variable with probability density function $f_X(x) = \\lambda e^{-\\lambda x}$ for $x > 0$. Write a function that samples this distribution (again using the inversion method). Compare a histogram with a plot of $f_X(x)$ to verify your function numerically. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3943887",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "66d91446",
   "metadata": {},
   "source": [
    "__(c)__ Let now $X$ have the **Pareto distribution** of **shape** $\\alpha > 0$ on $(b,\\infty)$, which has  probability density function $f_X(x) = \\alpha b^{\\alpha} x^{-\\alpha-1}$ for $x > b$. Write a function that samples this distribution (again using the inversion method). Compare a histogram with a plot of $f_X(x)$ to verify your function numerically. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e177f32d",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "47546d37",
   "metadata": {},
   "source": [
    "## 2.2 Central limit theorem? (4 Points)\n",
    "\n",
    "In this exercise we will have a closer look at central limits of the Pareto distribution, for which you implemented a random sampler in the previous exercise. By performing the appropriate integrals it is straightforward to show that \n",
    "$$ \\mathbb{E}[X] = \\begin{cases} \\infty & \\text{for }\\alpha \\leq 1 \\\\ \\frac{\\alpha b}{\\alpha - 1} & \\text{for }\\alpha > 1 \\end{cases}, \\qquad \\operatorname{Var}(X) = \\begin{cases} \\infty & \\text{for }\\alpha \\leq 2 \\\\ \\frac{\\alpha b^2}{(\\alpha - 1)^2(\\alpha-2)} & \\text{for }\\alpha > 2 \\end{cases}.$$\n",
    "This shows in particular that the distribution is **heavy tailed**, in the sense that some moments $\\mathbb{E}[X^k]$ diverge."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ccae582d",
   "metadata": {},
   "source": [
    "__(a)__ For $\\alpha = 4$, $b=1$ verify the central limit theorem numerically by comparing a suitably generated histogram of $Z_n = \\frac{\\sqrt{n}}{\\sigma_X}(\\bar{X}_n - \\mathbb{E}[X])$ to the standard normal distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82fe6efd",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "fe51f7b7",
   "metadata": {},
   "source": [
    "__(b)__ Consider now $X$ to have the Pareto distribution with $\\alpha = 3/2$ and $b=1$. Demonstrate numerically that \n",
    "\n",
    "$$\\varphi_X(t) = 1 + 3 i t - (|t|+i t)\\,\\sqrt{2\\pi|t|} + o(t^{3/2})$$\n",
    "or equivalently \n",
    "$$\\lim_{t\\to 0} \\frac{\\mathrm{Re}(\\varphi_X(t)) - 1}{- \\sqrt{2\\pi}|t|^{3/2}} = 1\\qquad\\text{and}\\qquad \\lim_{t\\to \\pm 0} \\frac{\\mathrm{Im}(\\varphi_X(t)) - 3t}{- t\\sqrt{2\\pi|t|}} = 1.$$\n",
    "_Hint_: perform numerical integration using [`scipy.integrate.quad`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quad.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20247ca4",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "6192f05d",
   "metadata": {},
   "source": [
    "__(c)__ Prove the **generalized CLT** for this particular distribution $X$ which states that $Z_n = c\\, n^{-\\gamma} \\sum_{i=1}^n (X_i - \\mathbb{E}[X_i])$ converges in distribution, for an appropriate choice of scaling exponent $\\gamma$ and overall constant $c$, to a limiting random variable $\\mathcal{S}$ with characteristic function $$\\varphi_{\\mathcal{S}}(t) = \\exp\\big(-(|t|+it)\\sqrt{|t|}\\big).$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7d4c6bc",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "5b1d9f54",
   "metadata": {},
   "source": [
    "__(d)__ The random variable $\\mathcal{S}$ has a stable Levy distribution with index $\\alpha = 3/2$ and skewness $\\beta = 1$. Its probability density function $f_{\\mathcal{S}}(x)$ does not admit a simple expression, but can be accessed numerically using SciPy's `scipy.stats.levy_stable.pdf(x,1.5,1.0)`. Verify numerically that the generalized CLT of (c) holds by comparing an appropriate histogram to this PDF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c1ac2a9",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "f49856d8",
   "metadata": {},
   "source": [
    "## 2.3 Joint probability density functions and sampling the normal distribution (3 Points)\n",
    "\n",
    "Let $\\Phi$ be a uniform random variable on $(0,2\\pi)$ and $R$ an independent continuous random variable with probability density function $f_R(r) = r\\,e^{-r^2/2}$ for $r>0$. Set $X = R \\cos \\Phi$ and $Y = R \\sin \\Phi$. This is called the **Box-Muller transform**.\n",
    "\n",
    "__(a)__ Since $\\Phi$ and $R$ are independent, the joint probability density of $\\Phi$ and $R$ is $f_{\\Phi,R}(\\phi,r) = f_\\Phi(\\phi)f_R(r) = \\frac{1}{2\\pi}\\, r\\,e^{-r^2/2}$. Show by change of variables that $X$ and $Y$ are also independent and both distributed as a standard normal distribution $\\mathcal{N}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8b250a81",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "5d064cef",
   "metadata": {},
   "source": [
    "__(b)__ Write a function to sample a pair of independent normal random variables using the Box-Muller transform. Hint: to sample $R$ you can use the inverse method of Exercise 2.1. Produce a histogram to check the distribution of (one of) your normal variables. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4023f99",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "35aaad93",
   "metadata": {},
   "source": [
    "## Feedback\n",
    "\n",
    "Please use the following Google form to give anonymous and constructive feedback on the lecture and/or these exercises. This is an easy way to contribute to improving the course: https://forms.gle/TznjCH74cvhmmJjKA"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
